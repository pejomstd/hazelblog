package se.ironic.hazelblog;

import com.hazelcast.core.MapLoader;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.enterprise.inject.spi.CDI;

/**
 *
 * @author pejo
 */
public class PostLoader implements MapLoader<Long, Post> {

    @Override
    public Post load(Long id) {
        PostRepository repo = CDI.current().select(PostRepository.class).get();
        Post post = repo.find(id);
        return post;
    }

    @Override
    public Map<Long, Post> loadAll(Collection<Long> ids) {
        Map<Long, Post> all = new HashMap<>();
        PostRepository repo = CDI.current().select(PostRepository.class).get();
        Map<String, Object> params = new HashMap<>();
        params.put("ids", ids);
        List<Post> posts = repo.findByQuery("SELECT p FROM Post p WHERE p.id in :ids", params);
        for (Post post : posts) {
            all.put(post.getId(), post);
            System.out.println("Post: " + post);
        }
        return all;
    }

    @Override
    public Iterable<Long> loadAllKeys() {
        PostRepository repo = CDI.current().select(PostRepository.class).get();
        List<Long> ids = repo.findByQuery("SELECT p.id FROM Post p", Collections.EMPTY_MAP);
        return ids;
    }

}
