/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package se.ironic.hazelblog;

import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author pejo
 */
@Stateless
@LocalBean
public class PostRepository {

    @PersistenceContext(name = "hazelblogPU")
    private EntityManager em;

    public Post find(Object key) {
        return em.find(Post.class, key);
    }
    
    public Post persist(Post post) {
        em.persist(post);
        return post;
    }
    
    public void delete(Post post) {
        Post merge = em.merge(post);
        em.remove(merge);
    }
    
    public <T> List<T> findByQuery(String jpql, Map<String, Object> params) {
        Query query = em.createQuery(jpql);
        for (Map.Entry<String, Object> entrySet : params.entrySet()) {
            String key = entrySet.getKey();
            Object value = entrySet.getValue();
            query.setParameter(key, value);
        }
        return query.getResultList();
    }
}
