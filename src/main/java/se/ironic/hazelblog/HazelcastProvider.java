package se.ironic.hazelblog;

import com.hazelcast.config.Config;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.core.HazelcastInstance;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Singleton;

/**
 *
 * @author pejo
 */
@ApplicationScoped
public class HazelcastProvider {

    private static final String INST_NAME_PREFIX = "HazelInst-";

    @Produces
    @Singleton
    @Hazelcast
    public HazelcastInstance create() {
        System.out.println("Creating Hazelcast Instance");
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        Config config = new Config();

        MapStoreConfig storeConfig = new MapStoreConfig();
        storeConfig.setEnabled(true);
        storeConfig.setClassName("se.ironic.hazelblog.PostLoader");
        config.getMapConfig("posts").setMapStoreConfig(storeConfig);
        HazelcastInstance newInst = com.hazelcast.core.Hazelcast.newHazelcastInstance(config);

        return newInst;
    }

    public void close(@Disposes @Hazelcast HazelcastInstance instance) {
        System.out.println("Shutting down Hazelcast....");
        instance.shutdown();
    }
}
