package se.ironic.hazelblog;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.query.EntryObject;
import com.hazelcast.query.PredicateBuilder;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author pejo
 */
@Path("posts")
@RequestScoped
public class PostResource {
    @Context
    UriInfo uriInfo;
    
    @Inject
    private PostRepository postRepository;

    @Inject
    @Hazelcast
    private HazelcastInstance hazelcastInstance;
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Collection<Post> list() {
        String author = uriInfo.getQueryParameters().getFirst("author");
        if(author != null) {
            System.out.println("Searching on map... author:" + author);
            IMap<Long, Post> map = hazelcastInstance.getMap("posts");
            EntryObject entryObject = new PredicateBuilder().getEntryObject();
            PredicateBuilder predicate = entryObject.get("author").equal(author);
            Collection<Post> values = map.values(predicate);
            return values;
        }
        return Collections.EMPTY_LIST;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Post get(@PathParam("id") Long id) {
        IMap<Long, Post> map = hazelcastInstance.getMap("posts");
        Post post = map.get(id);
        if(post == null) {
            throw new WebApplicationException("No post found!", Response.Status.NOT_FOUND);
        }
        return post;

    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void post(Post post) {
        postRepository.persist(post);
        // need to "touch" it to load it to the map.
        // could also have use put since we have id after persist.
        IMap<Long, Post> map = hazelcastInstance.getMap("posts");
        map.get(post.getId());
    }

    @DELETE
    @Path("{id}")
    public void delete(@PathParam("id") Long id) {
        IMap<Long, Post> map = hazelcastInstance.getMap("posts");
        Post post = map.remove(id);
        postRepository.delete(post);
    }
}
